package be.iris.dataclass;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Workday {
	private LocalDate currentDay;
	private List<Activity> lActivities = new ArrayList<Activity>();
	private int userID;
	private int breaktime;
	/**
	 * @param currentDay
	 * @param lActivities
	 * @param userID
	 * @param breaktime
	 */
	public Workday(LocalDate currentDay, List<Activity> lActivities, int userID, int breaktime) {
		this.currentDay = currentDay;
		this.lActivities = lActivities;
		this.userID = userID;
		this.breaktime = breaktime;
	}
	/**
	 * @return the currentDay
	 */
	public LocalDate getCurrentDay() {
		return currentDay;
	}
	/**
	 * @param currentDay the currentDay to set
	 */
	public void setCurrentDay(LocalDate currentDay) {
		this.currentDay = currentDay;
	}
	/**
	 * @return the lActivities
	 */
	public List<Activity> getlActivities() {
		return lActivities;
	}
	/**
	 * @param lActivities the lActivities to set
	 */
	public void setlActivities(List<Activity> lActivities) {
		this.lActivities = lActivities;
	}
	/**
	 * @return the userID
	 */
	public int getUserID() {
		return userID;
	}
	/**
	 * @param userID the userID to set
	 */
	public void setUserID(int userID) {
		this.userID = userID;
	}
	/**
	 * @return the breaktime
	 */
	public int getBreaktime() {
		return breaktime;
	}
	/**
	 * @param breaktime the breaktime to set
	 */
	public void setBreaktime(int breaktime) {
		this.breaktime = breaktime;
	}
	
}