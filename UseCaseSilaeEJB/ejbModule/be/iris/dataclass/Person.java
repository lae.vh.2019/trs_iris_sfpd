package be.iris.dataclass;
import java.io.Serializable;

public class Person implements Serializable {
	
	private static final long serialVersionUID = 4005920977817586288L;
	private String firstName;
	private String lastName;
	private String function;
	private int companyID;
	private String department;
	private String tel;
	private String sex;
	/**
	 * @param firstName
	 * @param lastName
	 * @param function
	 * @param companyID
	 * @param department
	 * @param tel
	 * @param sex
	 */
	public Person(String firstName, String lastName, String function, int companyID, String department, String tel,
			String sex) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.function = function;
		this.companyID = companyID;
		this.department = department;
		this.tel = tel;
		this.sex = sex;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the function
	 */
	public String getFunction() {
		return function;
	}
	/**
	 * @param function the function to set
	 */
	public void setFunction(String function) {
		this.function = function;
	}
	/**
	 * @return the companyID
	 */
	public int getCompanyID() {
		return companyID;
	}
	/**
	 * @param companyID the companyID to set
	 */
	public void setCompanyID(int companyID) {
		this.companyID = companyID;
	}
	/**
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}
	/**
	 * @param department the department to set
	 */
	public void setDepartment(String department) {
		this.department = department;
	}
	/**
	 * @return the tel
	 */
	public String getTel() {
		return tel;
	}
	/**
	 * @param tel the tel to set
	 */
	public void setTel(String tel) {
		this.tel = tel;
	}
	/**
	 * @return the sex
	 */
	public String getSex() {
		return sex;
	}
	/**
	 * @param sex the sex to set
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	
	
}
