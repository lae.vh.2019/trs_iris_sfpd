package be.iris.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;

import be.iris.navigator.MynavigatorUI;

public class MainView extends HorizontalLayout implements View {

	private Label label;

	public MainView() {
		setSizeFull();
		setSpacing(true);
		label = new Label("Entered in mainview.");
		addComponent(leftPanel());
	//	addComponent(headingLabel());
	//	addComponent(mainButton());
	}

	@Override
	public void enter(ViewChangeEvent event) {
		Notification.show("Showing view: Main!");

	}

	private Label headingLabel() {
		return new Label("Main");
	}

	private Button mainButton() {
		Button button = new Button("mainView", new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				getUI().getNavigator().navigateTo(MynavigatorUI.EMPMAN);

			}
		});
		return button;
	}

	private VerticalLayout leftPanel() {
		// Panel panel = new Panel("Astronomer Panel");
		// panel.setSizeUndefined(); // Shrink to fit content
		// Create the content

		VerticalLayout content = new VerticalLayout();
		content.addComponent(new Label("PersonName"));
		content.addComponent(new Label("JobTitle"));
		content.setMargin(true);

		return content;
	}

}