package be.iris.views;

import com.vaadin.navigator.View;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;

import be.iris.navigator.MynavigatorUI;

public class ActivityView extends VerticalLayout implements View {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4000415020169819349L;
	private String choice;
	private boolean flag = true;

	public ActivityView() {
		setSizeFull();
		setSpacing(true);
		Label lbActivity = new Label("Choose your activity please");

		HorizontalLayout HLButton = new HorizontalLayout();

		this.addComponent(lbActivity);
		HLButton.addComponent(breakButton());

		HLButton.addComponent(simpleActivityButton());
		HLButton.addComponent(projectActivityButton());
		this.addComponent(HLButton);
		if (flag) {
		this.addComponent(choiceActivityView.VLActivity(choice));}
		
	}

	private Button breakButton() {
		Button button = new Button("Break", new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				choice = "breaktime";
				
				flag = true;
				ActivityView acv = new ActivityView();
			}
		});
		return button;

	}

	private Button simpleActivityButton() {
		Button button = new Button("Simple activity", new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				choice = "simple";
				flag = true;
				ActivityView acv = new ActivityView();
			}
		});
		return button;

	}

	private Button projectActivityButton() {
		Button button = new Button("Project activity", new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				choice = "project";
				
				flag = true;
				ActivityView acv = new ActivityView();
			}
		});
		return button;

	}

	

}
