package be.iris.exceptions;

public class DatabaseConnectionFailedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5635194532915266126L;
	
	
	public DatabaseConnectionFailedException(){
		super();
	}
	public DatabaseConnectionFailedException(String message ){
		super(message);
	}

}
