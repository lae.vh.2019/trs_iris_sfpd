package be.iris.navigator;

//http://kushalbaldev.blogspot.com/2017/01/navigation-in-vaadin.html
import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.Navigator.ComponentContainerViewDisplay;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import be.iris.views.ActivityView;
import be.iris.views.EmployeeManagementView;
import be.iris.views.Login;
import be.iris.views.MainView;

@SuppressWarnings("serial")

public class MynavigatorUI extends UI {

 public Navigator navigator;
 public static final String MAINVIEW = "main";
 public static final String EMPMAN = "help";
 public static final String Activity = "act";


 @WebServlet(value = "/*", asyncSupported = true)
 @VaadinServletConfiguration(productionMode = false, ui = MynavigatorUI.class)
 public static class Servlet extends VaadinServlet {
 }

 @Override
 protected void init(VaadinRequest request) {
  final VerticalLayout layout = new VerticalLayout();
  layout.setMargin(true);
  setContent(layout);
  ComponentContainerViewDisplay viewDisplay= new ComponentContainerViewDisplay(layout);
  navigator=new Navigator(UI.getCurrent(),viewDisplay);
  navigator.addView("", new Login());
  navigator.addView(MAINVIEW, new MainView());
  navigator.addView(EMPMAN, new EmployeeManagementView());
  navigator.addView(Activity, new ActivityView());
  
 }

}