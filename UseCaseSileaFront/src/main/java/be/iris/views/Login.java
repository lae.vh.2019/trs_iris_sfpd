package be.iris.views;

import com.vaadin.navigator.View;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;

import be.iris.navigator.MynavigatorUI;

import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class Login extends VerticalLayout implements View {

	public Login() {
		setSizeFull();
		setSpacing(true);

		Label label = new Label("Enter your information below to log in.");
		TextField username = new TextField("username");
		TextField password = new TextField("password");

		addComponent(label);
		addComponent(username);
		addComponent(password);
		addComponent(loginButton());
		addComponent(actBt());

	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub

	}

	private Button loginButton() {
		Button button = new Button("Log In", new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				getUI().getNavigator().navigateTo(MynavigatorUI.MAINVIEW);

			}
		});
		return button;

	}
	private Button actBt() {
		Button button = new Button("ActivTESTTEST", new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				getUI().getNavigator().navigateTo(MynavigatorUI.Activity);

			}
		});
		return button;

	}
}
