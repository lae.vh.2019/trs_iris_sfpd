package be.iris.views;

import com.vaadin.navigator.View;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;

public class EmployeeManagementView extends VerticalLayout implements View {
 
 
 
 public EmployeeManagementView()
 {
  setSizeFull();
  setSpacing(true);
  addComponent(helpbutton());
  
  
  
 }

 @Override
 public void enter(ViewChangeEvent event) {
  Notification.show("Showing view: help!");
  }
 
 
 
 private Button helpbutton(){
  Button button = new Button("HelpView", new Button.ClickListener() {
   @Override
   public void buttonClick(ClickEvent event) {
    
   }
  });
  return button;
 

}

}