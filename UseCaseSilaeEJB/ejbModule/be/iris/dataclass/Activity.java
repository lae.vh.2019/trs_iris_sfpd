package be.iris.dataclass;

import java.util.*;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 
 */

/**
 * @author Silae
 *
 */

@Entity
@Table
public abstract class Activity {
	protected String nameActivity ; 
	protected Date startActivity ;
	protected Date endActivity;
	
	
	//constructor
	
	public Activity (){
		
	}

	/**
	 * @param nameActivity
	 * @param startActivity
	 * @param endActivity
	 */
	public Activity(String nameActivity, Date startActivity, Date endActivity) {
	
		this.nameActivity = nameActivity;
		this.startActivity = startActivity;
		this.endActivity = endActivity;
		}

	/**
	 * @return the name of a activity
	 */
	public String getNameActivity() {
		return nameActivity;
	}

	

	/**
	 * @return the start of the activity
	 */
	public Date getStartActivity() {
		return startActivity;
	}

	/**
	 * @param startActivity the startActivity to set
	 */
	public void setStartActivity(Date startActivity) {
		this.startActivity = startActivity;
	}

	/**
	 * @return the end of a activity
	 */
	public Date getEndActivity() {
		return endActivity;
	}

	/**
	 * @param endActivity the endActivity to set
	 */
	public void setEndActivity(Date endActivity) {
		this.endActivity = endActivity;
	}

	/**
	 * @return the listActivity
	 */


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Name of activity = " + nameActivity + ", hour of the beginning of activity = " + startActivity + ", hour of the end of activity = "
				+ endActivity + ".";
	}
	
	

	
}
