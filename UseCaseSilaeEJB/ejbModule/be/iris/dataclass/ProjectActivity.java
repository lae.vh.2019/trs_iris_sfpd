package be.iris.dataclass;

import java.util.Date;

public class ProjectActivity extends Activity{
	private Project project;
	
	public ProjectActivity(String nameActivity, Date startActivity, Date endActivity) {
		super(nameActivity, startActivity, endActivity);
		
	}

	/**
	 * @return the project
	 */
	public Project getProject() {
		return project;
	}

	/**
	 * @param project the project to set
	 */
	public void setProject(Project project) {
		this.project = project;
	}
	

}
