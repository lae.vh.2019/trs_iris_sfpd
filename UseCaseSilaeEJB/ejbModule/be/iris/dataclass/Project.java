package be.iris.dataclass;

import java.sql.Date;

public class Project {
	private String projectName;
	private Date startDate;
	private Date endDate;
	private boolean isActive;
	private Company companyOfProject;
	private int numberTimeProject;


	/**
	 * @param projectName
	 * @param startDate
	 * @param endDate
	 * @param isActive
	 * @param companyOfProject
	 * @param numberTimeProject
	 */
	public Project(String projectName, Date startDate, Date endDate, boolean isActive, Company companyOfProject,int numberTimeProject) {
		this.projectName = projectName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.isActive = isActive;
		this.companyOfProject = companyOfProject;
		this.numberTimeProject = numberTimeProject;
	}
	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}
	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	/**
	 * @return the numberTimeProject
	 */
	public int getNumberTimeProject() {
		return numberTimeProject;
	}
	/**
	 * @param numberTimeProject the numberTimeProject to set
	 */
	public void setNumberTimeProject(int numberTimeProject) {
		this.numberTimeProject = numberTimeProject;
	}
	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	/**
	 * @return the companyOfProject
	 */
	public Company getCompanyOfProject() {
		return companyOfProject;
	}
	/**
	 * @param companyOfProject the companyOfProject to set
	 */
	public void setCompanyOfProject(Company companyOfProject) {
		this.companyOfProject = companyOfProject;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	
	
}
