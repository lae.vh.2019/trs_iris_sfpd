package be.iris.service.users;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import be.iris.dataclass.*;
import be.iris.exceptions.DatabaseConnectionFailedException;

public class UserController {

	private static ObjectInputStream ois;


	private UserController() {
	}

	
	
	public static Boolean checkUser(String userName,String password) throws UnsupportedEncodingException, NoSuchAlgorithmException, DatabaseConnectionFailedException{
		Boolean result =true;
		out:for (User u : retrieveUsers()) {
			if (u.getUserName().equals(userName) && u.getPassWord().equals(Encryption.verifyPassword(password))) {
				result=true;
				break out;
			   }else {
			   result= false;
			}
		}
		
		if(!result){
			throw new DatabaseConnectionFailedException("userCredentials are wrong!!");
		}
		else{
			return true;
		}
		 
		
	}
	public static User[] retrieveUsers() {
		User[] users ={};
		try {
			ois = new ObjectInputStream(new FileInputStream("C:\\Users\\Duser\\Desktop\\Users.ser"));
			Object o = ois.readObject();
			users = ((User[])o);
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		return users;
		
	}
}
