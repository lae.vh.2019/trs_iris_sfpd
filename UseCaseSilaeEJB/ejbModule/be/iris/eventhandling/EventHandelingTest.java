
package be.iris.eventhandling;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.EventObject;
import java.util.List;

public class EventHandelingTest {
    public static void main(String[] args) {

        User2 silvio = new User2("silvio","123");
        Initiator2 loginPage = new Initiator2("LoginPage");
        loginPage.setListeners(silvio);
        silvio.setTries(3);
        if(silvio.getTries() == 3){
            loginPage.triggerTheEvent();
        }



    }

}
interface LoginFaildedListener extends EventListener {
    void loginFailedTooOften(NotificationEventLogin ne);
}
//-----------------------------------------------------------------------------------------
//Someone who says "Hello"
class User2 implements Serializable, LoginFaildedListener {

    /**
     *
     */
    //private static final long serialVersionUID = 8330903692569292413L;
    private String userName;
    private String passWord;


    private int tries = 0;

    public User2(String userName, String passWord) {
        this.userName = userName;
        this.passWord = passWord;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public int getTries() {
        return tries;
    }

    public void setTries(int tries) {
        this.tries = tries;
    }


    @Override
    public void loginFailedTooOften(NotificationEventLogin ne) {
        System.out.println(this.userName + ": " + ne.getReason());
    }
}


//-----------------------------------------------------------------------------------------
//Someone interested in "Hello" events
class Initiator2 {
    private String name;
    private List<LoginFaildedListener> listeners = new ArrayList<>();

    public Initiator2(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setListeners(LoginFaildedListener lfl) {
        this.listeners.add(lfl);
    }

    public void triggerTheEvent() {
        NotificationEventLogin ne = new NotificationEventLogin(this, "Too many tries");
        for (LoginFaildedListener lfl : listeners) {
            lfl.loginFailedTooOften(ne);
        }
    }
}

//-----------------------------------------------------------------------------------------
@SuppressWarnings("serial")
class NotificationEventLogin extends EventObject {
    private String reason;

    public NotificationEventLogin(Initiator2 source, String reason) {
        super(source);
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }
}

