package be.iris.dataclass;

public class Company {
private String street;
private int streetNumber;
private String town;
private String townNumber;
private String country;
private String tel;
private String vat;
private String bankNumber;
private int contactPersonNumber;
/**
 * @param street
 * @param streetNumber
 * @param town
 * @param townNumber
 * @param country
 * @param tel
 * @param vat
 * @param bankNumber
 * @param contactPersonNumber
 */
public Company(String street, int streetNumber, String town, String townNumber, String country, String tel, String vat,String bankNumber, int contactPersonNumber) {
	this.street = street;
	this.streetNumber = streetNumber;
	this.town = town;
	this.townNumber = townNumber;
	this.country = country;
	this.tel = tel;
	this.vat = vat;
	this.bankNumber = bankNumber;
	this.contactPersonNumber = contactPersonNumber;
}

/**
 * @return the contactPersonNumber
 */
public int getContactPersonNumber() {
	return contactPersonNumber;
}
/**
 * @param contactPersonNumber the contactPersonNumber to set
 */
public void setContactPersonNumber(int contactPersonNumber) {
	this.contactPersonNumber = contactPersonNumber;
}
/**
 * @return the street
 */
public String getStreet() {
	return street;
}
/**
 * @param street the street to set
 */
public void setStreet(String street) {
	this.street = street;
}
/**
 * @return the streetNumber
 */
public int getStreetNumber() {
	return streetNumber;
}
/**
 * @param streetNumber the streetNumber to set
 */
public void setStreetNumber(int streetNumber) {
	this.streetNumber = streetNumber;
}
/**
 * @return the town
 */
public String getTown() {
	return town;
}
/**
 * @param town the town to set
 */
public void setTown(String town) {
	this.town = town;
}
/**
 * @return the townNumber
 */
public String getTownNumber() {
	return townNumber;
}
/**
 * @param townNumber the townNumber to set
 */
public void setTownNumber(String townNumber) {
	this.townNumber = townNumber;
}
/**
 * @return the country
 */
public String getCountry() {
	return country;
}
/**
 * @param country the country to set
 */
public void setCountry(String country) {
	this.country = country;
}
/**
 * @return the tel
 */
public String getTel() {
	return tel;
}
/**
 * @param tel the tel to set
 */
public void setTel(String tel) {
	this.tel = tel;
}
/**
 * @return the vat
 */
public String getVat() {
	return vat;
}
/**
 * @param vat the vat to set
 */
public void setVat(String vat) {
	this.vat = vat;
}
/**
 * @return the bankNumber
 */
public String getBankNumber() {
	return bankNumber;
}
/**
 * @param bankNumber the bankNumber to set
 */
public void setBankNumber(String bankNumber) {
	this.bankNumber = bankNumber;
}



}
